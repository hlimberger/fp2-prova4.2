
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;
import utfpr.ct.dainf.pratica.Lancamento;
import utfpr.ct.dainf.pratica.ProcessaLancamentos;

/**
 * IF62C Fundamentos de Programação 2
 * Avaliação parcial.
 * @author 
 */
public class Pratica {
 
    public static void main(String[] args) throws FileNotFoundException, IOException {
        String caminho;
        int conta = 1;
        Scanner sc = new Scanner(System.in);
        System.out.print("Digite o caminho do arquivo: ");
        caminho = sc.next();
        ProcessaLancamentos pl = new ProcessaLancamentos(caminho);
        List<Lancamento> lista = new ArrayList<>();
        lista = pl.getLancamentos();
        
               
         do {
                System.out.print("Digite o numero da conta: ");
                conta = sc.nextInt();
                exibeLancamentosConta(lista,conta);
            } while (conta < 0);
        
    }
    
    public static void exibeLancamentosConta(List<Lancamento> lancamentos, Integer conta) throws IOException {
        for(Lancamento l : lancamentos){
            if(l.getConta() == conta){
                System.out.println(l.toString());
            }
        }
    }
 
}