package utfpr.ct.dainf.pratica;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.ArrayList;

/**
 * Linguagem Java
 * @author
 */
public class ProcessaLancamentos {
    private BufferedReader reader;

    public ProcessaLancamentos(File arquivo) throws FileNotFoundException {
        this.reader = new BufferedReader(new FileReader(arquivo));
    }

    public ProcessaLancamentos(String path) throws FileNotFoundException {
        this.reader = new BufferedReader(new FileReader(path));
    }
    
    private String getNextLine() throws IOException {
         return reader.readLine();
    }
    
    private Lancamento processaLinha(String linha) {
        Integer conta, ano, mes, dia;
        Date data;
        String descricao;
        Double valor;
        
        conta = Integer.parseInt(linha.substring(1, 6));
        ano = Integer.valueOf(linha.substring(7, 10));
        mes = Integer.valueOf(linha.substring(11, 12));  
        dia = Integer.valueOf(linha.substring(13, 14));
        GregorianCalendar gc = new GregorianCalendar(ano, mes, dia);
        data = gc.getTime();
        descricao = linha.substring(15,74);
        valor = Double.valueOf(linha.substring(75, 86)) / 100;
        
        Lancamento lancamento = new Lancamento(conta,data,descricao,valor);
        
        return lancamento;
    }
    
    private Lancamento getNextLancamento() throws IOException {
        
        String linha = this.getNextLine();
                
        return this.processaLinha(linha);
    }
    
    public List<Lancamento> getLancamentos() throws IOException {
        List<Lancamento> lista = new ArrayList<>();
        
        while(reader.ready()){
            lista.add(this.getNextLancamento());
        }
        
        LancamentoComparator lc = new LancamentoComparator();
        
        lista.sort(lc);
        
        return lista;
    }
    
}
